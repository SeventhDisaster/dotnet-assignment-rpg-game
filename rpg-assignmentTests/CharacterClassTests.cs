﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using rpg_assignment;

namespace rpg_assignmentTests
{
    public class CharacterClassTests
    {

        [Fact(DisplayName = "Create warrior with default attributes.")]
        public void Constructor_CreateWarrior_ShouldHaveProperDefaultAttributes()
        {
            Warrior warrior = new("Warrior");

            PrimaryAttribute expected = new(5, 2, 1, 10);
            PrimaryAttribute actual = warrior.TotalPrimary;

            Assert.Equal(expected, actual);
        }

        [Fact(DisplayName = "Create ranger with default attributes.")]
        public void Constructor_CreateRanger_ShouldHaveProperDefaultAttributes()
        {
            Ranger ranger = new("Ranger");

            PrimaryAttribute expected = new(1, 7, 1, 8);
            PrimaryAttribute actual = ranger.TotalPrimary;

            Assert.Equal(expected, actual);
        }

        [Fact(DisplayName = "Create rogue with default attributes.")]
        public void Constructor_CreateRogue_ShouldHaveProperDefaultAttributes()
        {
            Rogue rogue = new("Rogue");

            PrimaryAttribute expected = new(2, 6, 1, 8);
            PrimaryAttribute actual = rogue.TotalPrimary;

            Assert.Equal(expected, actual);
        }

        [Fact(DisplayName = "Create mage with default attributes.")]
        public void Constructor_CreateMage_ShouldHaveProperDefaultAttributes()
        {
            Mage mage = new("Mage");

            PrimaryAttribute expected = new(1, 1, 8, 5);
            PrimaryAttribute actual = mage.TotalPrimary;

            Assert.Equal(expected, actual);
        }
    }
}
