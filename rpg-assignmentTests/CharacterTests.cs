using System;
using Xunit;
using rpg_assignment;

namespace rpg_assignmentTests
{
    public class CharacterTests
    {
        [Fact (DisplayName = "Created character should be level 1")]
        public void Constructor_CreateSampleCharacter_ShouldShowCharacterLevelOne()
        {
            string sampleName = "Lancelot";
            int expected = 1;

            Character hero = new Warrior(sampleName);
            int actual = hero.Level;

            Assert.Equal(expected, actual);
        }

        [Fact(DisplayName = "Character levels should increase on levelup")]
        public void LevelUp_IncreaseCharacterLevelByOne_ShouldReturnLevelTwo()
        {
            string sampleName = "Lancelot";
            int expected = 2;

            Character hero = new Warrior(sampleName);
            hero.LevelUp();
            int actual = 2;

            Assert.Equal(expected, actual);
        }

        [Theory(DisplayName = "ArgumentException should be thrown on attempted level down or zero")]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_PassNegativeArgument_ShouldThrowArgumentException(int levelCount)
        {
            string sampleName = "Lancelot";
            Character hero = new Warrior(sampleName);

            Assert.Throws<ArgumentException>(() => hero.LevelUp(levelCount));
        }
    }
}
