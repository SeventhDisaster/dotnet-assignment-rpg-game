﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rpg_assignment;
using Xunit;

namespace rpg_assignmentTests
{
    public class WeaponTests
    {
        [Fact(DisplayName = "Should throw InvalidWeaponException when equipping high level weapon.")]
        public void Equip_GiveHighLevelWeaponToWarrior_ShouldThrowInvalidWeaponException()
        {
            Warrior warrior = new("Warrior");

            string name = "Steel Axe";
            WeaponType type = WeaponType.AXE;
            WeaponAttribute attributes = new(10, 1.1);
            int requiredLevel = 2;

            Weapon axe = new(name, type, attributes, requiredLevel);

            Assert.Throws<InvalidWeaponException>(() => warrior.Equip(axe));
        }

        [Fact(DisplayName = "Should throw InvalidWeaponException when equipping wrong type of weapon")]
        public void Equip_GiveWeaponOfTypeBowToWarrior_ShouldThrowInvalidWeaponException()
        {
            Warrior warrior = new("Warrior");

            string name = "Common Bow";
            WeaponType type = WeaponType.BOW;
            WeaponAttribute attributes = new(6, 1.1);
            int requiredLevel = 1;
            Weapon bow = new(name, type, attributes, requiredLevel);

            Assert.Throws<InvalidWeaponException>(() => warrior.Equip(bow));
        }

        [Fact(DisplayName = "Should return success message upon successful weapon equip")]
        public void Equip_GiveWeaponOfTypeSwordToWarrior_ShouldReturnSuccessMessage()
        {
            Warrior warrior = new("Warrior");

            string name = "Common Sword";
            WeaponType type = WeaponType.SWORD;
            WeaponAttribute attributes = new(6, 1.2);
            int requiredLevel = 1;
            Weapon sword = new(name, type, attributes, requiredLevel);

            string expected = $"{name} equipped!";
            string actual = warrior.Equip(sword);

            Assert.Equal(expected, actual);
        }
    }
}
