﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rpg_assignment;
using Xunit;

namespace rpg_assignmentTests
{
    public class DPSTests
    {
        [Fact(DisplayName = "Should calculate DPS with no weapon equipped.")]
        public void Attack_DoAttackWithNoWeaponEquipped_ShouldReturnOnePointZeroFive()
        {
            Warrior warrior = new("Warrior");

            double expected = 1.0 * (1.0 + (5.0 / 100.0));
            double actual = warrior.Attack();

            Assert.Equal(expected, actual);
        }

        [Fact(DisplayName = "Should calculated DPS with weapon equipped.")]
        public void Attack_DoAttackWithWeaponEquipped_ShouldReturnCalculatedDamage()
        {
            Warrior warrior = new("Warrior");

            string name = "Common Axe";
            WeaponType type = WeaponType.AXE;
            WeaponAttribute attributes = new(7, 1.1);
            int requiredLevel = 1;
            Weapon sword = new(name, type, attributes, requiredLevel);
            warrior.Equip(sword);

            double expected = (7.0 * 1.1) * (1.0 + (5.0 / 100.0));
            Console.WriteLine(expected);
            double actual = warrior.Attack();

            Assert.Equal(expected, actual);
        }
        
        [Fact(DisplayName = "Should calculated DPS with weapon and armor equipped.")]
        public void Attack_DoAttackWithWeaponAndArmorEquipped_ShouldReturnCalculatedDamage()
        {
            Warrior warrior = new("Warrior");

            string weaponName = "Common Axe";
            WeaponType weaponType = WeaponType.AXE;
            WeaponAttribute weaponAttributes = new(7, 1.1);
            int requiredWeaponLevel = 1;
            Weapon sword = new(weaponName, weaponType, weaponAttributes, requiredWeaponLevel);
            warrior.Equip(sword);

            string armorName = "Common Plate Armor";
            ArmorType armorType = ArmorType.PLATE;
            ItemSlot armorSlot = ItemSlot.BODY;
            PrimaryAttribute armorAttribute = new() { Strength = 1, Vitality = 2 };
            int requiredArmorLevel = 1;
            Armor plate = new(armorName, armorType, armorSlot, armorAttribute, requiredArmorLevel);
            warrior.Equip(plate);


            double expected = (7.0 * 1.1) * (1.0 + ((5.0 + 1.0) / 100.0));
            Console.WriteLine(expected);
            double actual = warrior.Attack();

            Assert.Equal(expected, actual);
        }
    }
}
