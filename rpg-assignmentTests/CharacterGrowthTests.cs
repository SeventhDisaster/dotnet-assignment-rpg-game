﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rpg_assignment;
using Xunit;

namespace rpg_assignmentTests
{
    public class CharacterGrowthTests
    {
        [Fact(DisplayName = "Increase Warrior stats when level up")]
        public void Constructor_CreateAndLevelWarrior_ShouldIncreaseAttributesAccordingly()
        {
            Warrior warrior = new("Warrior");

            PrimaryAttribute basic = new(5, 2, 1, 10);
            PrimaryAttribute gain = new(3, 2, 1, 5);
            PrimaryAttribute expected = basic + gain;

            warrior.LevelUp();

            PrimaryAttribute actual = warrior.TotalPrimary;

            Assert.Equal(expected, actual);
        }

        [Fact(DisplayName = "Increase Ranger stats when level up")]
        public void Constructor_CreateAndLevelRanger_ShouldIncreaseAttributesAccordingly()
        {
            Ranger ranger = new("Ranger");

            PrimaryAttribute basic = new(1, 7, 1, 8);
            PrimaryAttribute gain = new(1, 5, 1, 2);
            PrimaryAttribute expected = basic + gain;

            ranger.LevelUp();

            PrimaryAttribute actual = ranger.TotalPrimary;

            Assert.Equal(expected, actual);
        }

        [Fact(DisplayName = "Increase Rogue stats when level up")]
        public void Constructor_CreateAndLevelRogue_ShouldIncreaseAttributesAccordingly()
        {
            Rogue rogue = new("Rogue");

            PrimaryAttribute basic = new(2, 6, 1, 8);
            PrimaryAttribute gain = new(1, 4, 1, 3);
            PrimaryAttribute expected = basic + gain;

            rogue.LevelUp();

            PrimaryAttribute actual = rogue.TotalPrimary;

            Assert.Equal(expected, actual);
        }

        [Fact(DisplayName = "Increase Mage stats when level up")]
        public void Constructor_CreateAndLevelMage_ShouldIncreaseAttributesAccordingly()
        {
            Mage mage = new("Mage");

            PrimaryAttribute basic = new(1, 1, 8, 5);
            PrimaryAttribute gain = new(1, 1, 5, 3);
            PrimaryAttribute expected = basic + gain;

            mage.LevelUp();

            PrimaryAttribute actual = mage.TotalPrimary;

            Assert.Equal(expected, actual);
        }

        [Fact(DisplayName = "Secondary stats are calculated from leveled character")]
        public void Constructor_CreateAndLevelWarrior_ShouldIncreaseSecondaryAttributesAlongWithTotal()
        {
            Warrior warrior = new("Warrior");
            warrior.LevelUp();

            PrimaryAttribute basic = new(5, 2, 1, 10);
            PrimaryAttribute gain = new(3, 2, 1, 5);
            PrimaryAttribute totalPrimary = basic + gain;
            SecondaryAttribute expected = new(totalPrimary);


            SecondaryAttribute actual = warrior.TotalSecondary;

            Assert.Equal(expected, actual);
        }
    }
}
