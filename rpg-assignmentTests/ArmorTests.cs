﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rpg_assignment;
using Xunit;

namespace rpg_assignmentTests
{
    public class ArmorTests
    {
        [Fact(DisplayName = "Should throw InvalidArmorException when equipping high level armor.")]
        public void Equip_GiveHighLevelArmorToWarrior_ShouldThrowInvalidArmorException()
        {
            Warrior warrior = new("Warrior");

            string name = "Steel Chainmail Armor";
            ArmorType type = ArmorType.MAIL;
            ItemSlot slot = ItemSlot.BODY;
            PrimaryAttribute attribute = new() { Strength = 2, Vitality = 3 };
            int requiredLevel = 2;

            Armor chainmail = new(name, type, slot, attribute, requiredLevel);

            Assert.Throws<InvalidArmorException>(() => warrior.Equip(chainmail));
        }

        [Fact(DisplayName = "Should throw InvalidArmorException when equipping wrong type of armor")]
        public void Equip_GiveArmorOfTypeClothToWarrior_ShouldThrowInvalidArmorException()
        {
            Warrior warrior = new("Warrior");

            string name = "Common Cloth Armor";
            ArmorType type = ArmorType.CLOTH;
            ItemSlot slot = ItemSlot.BODY;
            PrimaryAttribute attribute = new() { Intelligence = 3, Vitality = 1 };
            int requiredLevel = 1;

            Armor cloth = new(name, type, slot, attribute, requiredLevel);

            Assert.Throws<InvalidArmorException>(() => warrior.Equip(cloth));
        }

        [Fact(DisplayName = "Should return success message upon successful armor equip")]
        public void Equip_GiveWeaponOfTypeSwordToWarrior_ShouldReturnSuccessMessage()
        {
            Warrior warrior = new("Warrior");

            string name = "Common Plate Armor";
            ArmorType type = ArmorType.PLATE;
            ItemSlot slot = ItemSlot.BODY;
            PrimaryAttribute attribute = new() { Strength = 1, Vitality = 2 };
            int requiredLevel = 1;

            Armor plate = new(name, type, slot, attribute, requiredLevel);

            string expected = $"{name} equipped!";
            string actual = warrior.Equip(plate);

            Assert.Equal(expected, actual);
        }
    }
}
