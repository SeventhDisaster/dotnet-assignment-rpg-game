﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using rpg_assignment;

namespace rpg_assignmentTests
{
    public class PrimaryAttributeTests
    {
        [Fact(DisplayName = "Create primary attribute")]
        public void Constructor_CreatePrimaryAttribute_ShouldReturnFilledAttributes()
        {
            int strength = 1;
            int dexterity = 2;
            int intelligence = 3;
            int vitality = 4;

            PrimaryAttribute actual = new(strength, dexterity, intelligence, vitality);

            Assert.Equal(strength, actual.Strength);
            Assert.Equal(dexterity, actual.Dexterity);
            Assert.Equal(intelligence, actual.Intelligence);
            Assert.Equal(vitality, actual.Vitality);
        }

        [Fact(DisplayName = "Add primary attributes together operator override")]
        public void AddOperatorOverride_AddTwoPrimaryAttributeSets_ShouldReturnSumOfAllAttributes()
        {
            int strA = 3;
            int dexA = 3;
            int intA = 3;
            int vitA = 3;

            PrimaryAttribute attributeA = new(strA, dexA, intA, vitA);

            int strB = 4;
            int dexB = 4;
            int intB = 4;
            int vitB = 4;

            PrimaryAttribute attributeB = new(strB, dexB, intB, vitB);


            PrimaryAttribute expected = new(strA+strB, dexA+dexB, intA+intB, vitA+vitB);
            PrimaryAttribute actual = attributeA + attributeB;

            Assert.Equal(expected, actual);
        }
        
        [Fact(DisplayName = "Subtract primary attributes together operator override")]
        public void SubtractOperatorOverride_SubtractTwoPrimaryAttributeSets_ShouldReturnDifferenceBetweenAttributes()
        {
            int strA = 3;
            int dexA = 3;
            int intA = 3;
            int vitA = 3;

            PrimaryAttribute attributeA = new(strA, dexA, intA, vitA);

            int strB = 4;
            int dexB = 4;
            int intB = 4;
            int vitB = 4;

            PrimaryAttribute attributeB = new(strB, dexB, intB, vitB);


            PrimaryAttribute expected = new(strA-strB, dexA-dexB, intA-intB, vitA-vitB);
            PrimaryAttribute actual = attributeA - attributeB;

            Assert.Equal(expected, actual);
        }        
        
        [Fact(DisplayName = "Multiply value of primary attributes override")]
        public void MultiplyOperatorOverride_MultiplyValuesOfPrimaryAttributes_ShouldReturnProductOfMultiplication()
        {
            int strength = 3;
            int dexterity = 3;
            int intelligence = 3;
            int vitality = 3;

            int multiplier = 2;

            PrimaryAttribute attribute = new(strength, dexterity, intelligence, vitality);


            PrimaryAttribute expected = new(strength * multiplier, dexterity * multiplier, intelligence * multiplier, vitality * multiplier);
            PrimaryAttribute actual = attribute * multiplier;

            Assert.Equal(expected, actual);
        }
    }
}
