﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace rpg_assignment
{
    public enum WeaponType
    {
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND
    }
    public class Weapon : Item
    {
        /// <summary>
        /// Defines the type of weapon. Certain classes can only wield certain weapon types.
        /// </summary>
        public WeaponType Type { get; set; }

        /// <summary>
        /// The WeaponAttributes of the weapon. Damage and Attack speed. Used to determine DPS
        /// </summary>
        public WeaponAttribute Attributes { get; set; }


        /// <summary>
        /// Constructor for a new weapon. Expects all values to be passed in. No defauls.
        /// </summary>
        /// <param name="name">Name of the weapon</param>
        /// <param name="type">Type of weapon. WeaponType Enumerator</param>
        /// <param name="attributes">WeaponAttributes: Damage and attackspeed</param>
        /// <param name="reqLevel">Required level to equip weapon.</param>
        public Weapon (string name, WeaponType type, WeaponAttribute attributes, int reqLevel) : base(name, reqLevel, ItemSlot.WEAPON)
        {
            Type = type;
            Attributes = attributes;
        }

        public override string ToString()
        {
            return $"== Weapon ==\n" +
                $"Name: {Name}\n" +
                $"Type: {Type}\n" +
                $"Required Level: {ReqLevel}\n" +
                $"Stats:\n" +
                $"-- Weapon Damage: {Attributes.Damage}\n" +
                $"-- Attack Speed: {Attributes.AttackSpeed}\n" +
                $"============";
        }
    }
}
