﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_assignment
{
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException()
        {

        }
        /// <summary>
        /// Creates the InvalidArmorException with a message passed in
        /// </summary>
        /// <param name="message">Message to display on exception throw</param>
        public InvalidArmorException(string message) : base(message)
        {

        }
        public InvalidArmorException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
