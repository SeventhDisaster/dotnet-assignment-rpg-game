﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_assignment
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException()
        {

        }

        /// <summary>
        /// Creates an InvalidWeaponException with a message passed in
        /// </summary>
        /// <param name="message">Message to display on exception throw</param>
        public InvalidWeaponException(string message) : base(message)
        {

        }        
        public InvalidWeaponException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
