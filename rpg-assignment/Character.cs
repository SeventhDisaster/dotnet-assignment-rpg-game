﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_assignment
{
    abstract public class Character
    {

        public string Name { get; set; }
        public int Level { get; set; }

        /// <summary>
        /// Retrieves the classname of the character, implemented on each character class
        /// </summary>
        abstract public string ClassName { get; }

        /// <summary>
        /// Base stats set on character creation at level 1.
        /// </summary>
        public PrimaryAttribute BasePrimary { get; set; }

        /// <summary>
        /// Readonly: Retrieves a list of allowed weapon types on the class. Implemented on each class.
        /// </summary>
        abstract public List<WeaponType> AllowedWeaponTypes { get; }

        /// <summary>
        /// Readonly: Retrieves a list of allowed armor types on the class. Implemented on each class.
        /// </summary>
        abstract public List<ArmorType> AllowedArmorTypes { get;  }

        /// <summary>
        /// Readonly: Retrieves the amount of base stats (PrimaryAttributes) earned on level-up. Implemented on each class.
        /// </summary>
        abstract public PrimaryAttribute GrowthRate { get; }

        /// <summary>
        /// Attempts to retrieve the stats of a piece of armor.
        /// </summary>
        /// <param name="slot">Required. Determines which slot to get the attributes from</param>
        /// <returns>Returns the primary attributes of provided armor slot. Returns primary attributes of all 0 if the key is null.</returns>
        private PrimaryAttribute GetEquippedPrimaries(ItemSlot slot)
        {
            if(equipment.TryGetValue(slot, out Item item))
            {
                Armor piece = (Armor)item;
                return piece.Attributes;
            } else
            {
                return new();
            }
        }

        /// <summary>
        /// Calculates total primary from base + armor on get call
        /// </summary>
        public PrimaryAttribute TotalPrimary
        {
            get
            {
                PrimaryAttribute armorBonus = GetEquippedPrimaries(ItemSlot.HEAD) + GetEquippedPrimaries(ItemSlot.BODY) + GetEquippedPrimaries(ItemSlot.LEGS);
                return BasePrimary + armorBonus;
            }
        } 

        /// <summary>
        /// Calculates total secondary from total primary on get call
        /// </summary>
        public SecondaryAttribute TotalSecondary 
        { 
            get
            {
                return new SecondaryAttribute(TotalPrimary);
            }
        } 

        /// <summary>
        /// Defines the equipment window of a character.
        /// </summary>
        public Dictionary<ItemSlot, Item> equipment = new();

        /// <summary>
        /// Creates a character with provided name. New characters are always created at level 1.
        /// </summary>
        /// <param name="name">Required. Defines the name of the character.</param>
        public Character(string name)
        {
            Name = name;
            Level = 1;
        }

        /// <summary>
        /// Levels up a character and increases that character's base stats by it's corresponding growth rate
        /// Throws argument exception when passed 0 or lower numbers.
        /// </summary>
        /// <param name="amount">Optional. Defines the amount of levels to gain. Default is 1</param>
        public void LevelUp(int amount)
        {
            if(amount < 1)
            {
                throw new ArgumentException("Negative values or zero not allowed.");
            }
            Level += amount;
            BasePrimary += (GrowthRate * amount);
        }

        public void LevelUp()
        {
            LevelUp(1);
        }

        /// <summary>
        /// Calculates the damage of a character based on their class' damage scaling and stats NOT including equipped weapon.
        /// For example, a Warrior's damage is calculated based on strength. This is implemented on the sub-classes
        /// </summary>
        /// <returns>Returns a double of the expected damage not including weapon</returns>
        abstract public double CalculateDamage();

        /// <summary>
        /// Calculates the total amount of damage (DPS) based on stats and the equipped weapon.
        /// </summary>
        /// <returns>Returns the total DPS calculation based on every factor.</returns>
        public double Attack()
        {
            try
            {
                Weapon weapon = (Weapon)equipment[ItemSlot.WEAPON];
                return (weapon.Attributes.Damage * weapon.Attributes.AttackSpeed) * CalculateDamage();
            }
            catch (KeyNotFoundException)
            {
                // If key for weapon was not found, no weapon was equipped, therefore the DPS is 1
                return 1.0 * CalculateDamage();
            }
        }

        /// <summary>
        /// Equips the character with a provided item and tosses away the existing item.
        /// Also checks item's level requirement.
        /// Throws InvalidWeaponException or InvalidArmorException if level requirement is too high for the character's level
        /// </summary>
        /// <param name="item">Which item to equip, usually called by it's overloaded methods</param>
        /// <returns>Returns a string that says: "[Item name] equipped!"</returns>
        public string Equip(Item item)
        {
            if (item.ReqLevel > Level)
            {
                throw
                    item.Slot == ItemSlot.WEAPON ?
                    new InvalidWeaponException($"Character does not meet required level to equip weapon: {item.Name}")
                    :
                    new InvalidArmorException($"Character does not meet required level to equip armor: {item.Name}");
            }

            equipment[item.Slot] = item; // Always overwrite and replace item. Old item gets tossed away

            return $"{item.Name} equipped!";
        }

        /// <summary>
        /// Overloads regular equip, specific for weapons. Handles check to see if character's class is allowed to hold the type of weapon
        /// Throws InvalidWeaponException if illegal
        /// </summary>
        /// <param name="weapon">The weapon to equip</param>
        /// <returns>Returns a string that says: "[Item name] equipped!"</returns>
        public string Equip(Weapon weapon)
        {
            if (!AllowedWeaponTypes.Contains(weapon.Type))
            {
                throw new InvalidWeaponException($"Character is not allowed to equip weapons of type: {weapon.Type}");
            }

            return Equip((Item)weapon);
        }

        /// <summary>
        /// Overloads regular equip, specific for armor. Handles check to see if character's class is allowed to wear the type of armor
        /// Throws InvalidArmorException if illegal
        /// </summary>
        /// <param name="armor">The armor piece to equip</param>
        /// <returns>Returns a string that says: "[Item name] equipped!"</returns>
        public string Equip(Armor armor)
        {
            if (!AllowedArmorTypes.Contains(armor.Type))
            {
                throw new InvalidArmorException($"Character is not allowed to equip armor of type: {armor.Type}");
            }

            return Equip((Item)armor);
        }

        /// <summary>
        /// Creates a character card with information about the instance of the character.
        /// </summary>
        /// <returns>A string containing details about the character</returns>
        override public string ToString()
        {
            return $"======= Character Info =======\n" +
                $"Name: {Name}\n" +
                $"Class: {ClassName}\n" +
                $"Level: {Level}\n" +
                $"HP: {TotalSecondary.Health}\n" +
                $"Stats:\n" +
                $"-- STR: {TotalPrimary.Strength} (+{(TotalPrimary - BasePrimary).Strength})\n" +
                $"-- DEX: {TotalPrimary.Dexterity} (+{(TotalPrimary - BasePrimary).Dexterity})\n" +
                $"-- INT: {TotalPrimary.Intelligence} (+{(TotalPrimary - BasePrimary).Intelligence})\n" +
                $"-- VIT: {TotalPrimary.Vitality} (+{(TotalPrimary - BasePrimary).Vitality})\n" +
                $"Armor Rating: {TotalSecondary.ArmorRating}\n" +
                $"Elemental Resistance: {TotalSecondary.ElementalResistance}\n" +
                $"=================================";
        }
    }
}
