﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_assignment
{
    public enum ArmorType
    {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }
    public class Armor : Item
    {
        /// <summary>
        /// Defines the type of armor it is. Certain armor types can only be equipped by certain classes
        /// </summary>
        public ArmorType Type { get; set; }

        /// <summary>
        /// Defines the stats gained by successfully equipping the armor.
        /// </summary>
        public PrimaryAttribute Attributes { get; set; }

        /// <summary>
        /// Creates a new armor. Expects all details in the constructor
        /// </summary>
        /// <param name="name">Name of the piece of armor</param>
        /// <param name="type">What the type of armor is. ArmorType Enumerator</param>
        /// <param name="slot">Which slot the piece of armor belongs to. ItemSlot Enumerator</param>
        /// <param name="attributes">PrimaryAttributes gained by equipping armor</param>
        /// <param name="reqLevel">Required level to equip armor</param>
        public Armor(string name, ArmorType type, ItemSlot slot, PrimaryAttribute attributes, int reqLevel) : base(name, reqLevel, slot)
        {
            if(slot == ItemSlot.WEAPON)
            {
                throw new ArgumentException("Armor cannot be of slot-type WEAPON");
            }
                
            Attributes = attributes;
            Type = type;
        }

        public override string ToString()
        {
            return $"== Armor ==\n" +
                $"Name: {Name}\n" +
                $"Slot: {Slot}\n" +
                $"Type: {Type}\n" +
                $"Required Level: {ReqLevel}\n" +
                $"Stats:\n" +
                $"-- STR: +{Attributes.Strength}\n" +
                $"-- DEX: +{Attributes.Dexterity}\n" +
                $"-- INT: +{Attributes.Intelligence}\n" +
                $"-- VIT: +{Attributes.Vitality}\n" +
                $"============";
        }
    }
}
