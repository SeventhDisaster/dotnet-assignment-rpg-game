﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_assignment
{
    public class PrimaryAttribute
    {
        
        public int Strength { get; set; } = 0; 
        public int Dexterity { get; set; } = 0;
        public int Intelligence { get; set; } = 0;
        public int Vitality { get; set; } = 0;

        /// <summary>
        /// Creates a base set of PrimaryAttribute stats. Defaults all stats to 0.
        /// </summary>
        public PrimaryAttribute()
        {

        }

        /// <summary>
        /// Creates a set of stats with provided values.
        /// </summary>
        /// <param name="strength">Points of STR</param>
        /// <param name="dexterity">Points of DEX</param>
        /// <param name="intelligence">Points of INT</param>
        /// <param name="vitality">Points of VIT</param>
        public PrimaryAttribute(int strength, int dexterity, int intelligence, int vitality)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
            Vitality = vitality;
        }

        /// <summary>
        /// Overrides the plus operator for PrimaryAttributes.
        /// Allows attributes to be easily added together for calculations,
        /// </summary>
        /// <param name="sourceA">Left hand side of calculation</param>
        /// <param name="sourceB">Right hand side of calculation</param>
        /// <returns></returns>
        public static PrimaryAttribute operator +(PrimaryAttribute sourceA, PrimaryAttribute sourceB)
        {
            return new PrimaryAttribute
            {
                Strength = sourceA.Strength + sourceB.Strength,
                Dexterity = sourceA.Dexterity + sourceB.Dexterity,
                Intelligence = sourceA.Intelligence + sourceB.Intelligence,
                Vitality = sourceA.Vitality + sourceB.Vitality
            };
        }

        /// <summary>
        /// Overrides the minus operator for PrimaryAttributes.
        /// Allows for easy subtraction of primary attributes. 
        /// </summary>
        /// <param name="sourceA">Left hand side of calculation</param>
        /// <param name="sourceB">Right hand side of calculation</param>
        /// <returns></returns>
        public static PrimaryAttribute operator -(PrimaryAttribute sourceA, PrimaryAttribute sourceB)
        {
            return new PrimaryAttribute
            {
                Strength = sourceA.Strength - sourceB.Strength,
                Dexterity = sourceA.Dexterity - sourceB.Dexterity,
                Intelligence = sourceA.Intelligence - sourceB.Intelligence,
                Vitality = sourceA.Vitality - sourceB.Vitality
            };
        }       
        /// <summary>
        /// Overrides the times operator for PrimaryAttributes, allowing for easy multiplication of stats.
        /// </summary>
        /// <param name="source">Factor</param>
        /// <param name="multiplier">Multiplier</param>
        /// <returns></returns>
        public static PrimaryAttribute operator *(PrimaryAttribute source, int multiplier)
        {
            return new PrimaryAttribute
            {
                Strength = source.Strength * multiplier,
                Dexterity = source.Dexterity * multiplier,
                Intelligence = source.Intelligence * multiplier,
                Vitality = source.Vitality * multiplier,
            };
        }

        public override bool Equals(object obj)
        {
            return obj is PrimaryAttribute attribute &&
                   Strength == attribute.Strength &&
                   Dexterity == attribute.Dexterity &&
                   Intelligence == attribute.Intelligence &&
                   Vitality == attribute.Vitality;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Strength, Dexterity, Intelligence, Vitality);
        }
    }
}
