﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_assignment
{
    public class SecondaryAttribute
    {
        public int Health { get; set; } = 0;
        public int ArmorRating { get; set; } = 0;
        public int ElementalResistance { get; set; } = 0;
        
        /// <summary>
        /// Calculates a set of SecondaryAttributes based on an existing PrimaryAttribute amount.
        /// </summary>
        /// <param name="totalPrimary">Base of PrimaryAttribute for calculation</param>
        public SecondaryAttribute(PrimaryAttribute totalPrimary)
        {
            Health = totalPrimary.Vitality * 10;
            ArmorRating = (totalPrimary.Dexterity) + (totalPrimary.Strength);
            ElementalResistance = totalPrimary.Intelligence;
        }

        public override bool Equals(object obj)
        {
            return obj is SecondaryAttribute attribute &&
                   Health == attribute.Health &&
                   ArmorRating == attribute.ArmorRating &&
                   ElementalResistance == attribute.ElementalResistance;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Health, ArmorRating, ElementalResistance);
        }

        public static bool operator ==(SecondaryAttribute left, SecondaryAttribute right)
        {
            return EqualityComparer<SecondaryAttribute>.Default.Equals(left, right);
        }

        public static bool operator !=(SecondaryAttribute left, SecondaryAttribute right)
        {
            return !(left == right);
        }
    }
}
