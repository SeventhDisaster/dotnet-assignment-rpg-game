﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_assignment
{
    public class WeaponAttribute
    {
        public int Damage { get; set; }
        public double AttackSpeed { get; set; }

        /// <summary>
        /// Creates a basic set of weapon attributes.
        /// Used in DPS calculations, can vary on different weapons.
        /// </summary>
        /// <param name="damage">The base damage of the weapon</param>
        /// <param name="attackSpeed">The base attackspeed of the weapon</param>
        public WeaponAttribute(int damage, double attackSpeed)
        {
            Damage = damage;
            AttackSpeed = attackSpeed;
        }
    }
}
