﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_assignment
{
    public enum ItemSlot
    {
        WEAPON,
        HEAD,
        BODY,
        LEGS
    }
    public abstract class Item
    {
        /// <summary>
        /// Name of the item
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Level required to equip the item
        /// </summary>
        public int ReqLevel { get; set; }

        /// <summary>
        /// Which slot the item belongs in. ItemSlot Enumerator.
        /// </summary>
        public ItemSlot Slot { get; set; }

        /// <summary>
        /// Item constructor. Called from sub-classes
        /// </summary>
        /// <param name="name">Name of the item</param>
        /// <param name="reqLevel">Required level to equip item.</param>
        /// <param name="slot">Equipment slot for the item. ItemSlot Enumerator</param>
        public Item (string name, int reqLevel, ItemSlot slot)
        {
            Name = name;
            ReqLevel = reqLevel;
            Slot = slot;
        }

        public abstract override string ToString();
    }
}
