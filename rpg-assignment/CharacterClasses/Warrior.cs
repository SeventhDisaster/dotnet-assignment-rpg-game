﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_assignment
{
    public class Warrior : Character
    {
        /// <summary>
        /// Returns the class-name of the character
        /// </summary>
        public override string ClassName => "Warrior";

        /// <summary>
        /// Defines the attributes earned on level-up of a warrior
        /// 3 STR, 2 DEX, 1 INT, 5 VIT
        /// </summary>
        public override PrimaryAttribute GrowthRate => new(3, 2, 1, 5);

        /// <summary>
        /// Defines the allowed weapons of warrior
        /// Swords, Hammers an Axes
        /// </summary>
        public override List<WeaponType> AllowedWeaponTypes
        {
            get
            {
                return new List<WeaponType> { WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD };
            }
        }        
        
        /// <summary>
        /// Defines the allowed armor types for a warrior
        /// Mail and Plate
        /// </summary>
        public override List<ArmorType> AllowedArmorTypes
        {
            get
            {
                return new List<ArmorType> { ArmorType.MAIL, ArmorType.PLATE };
            }
        }

        /// <summary>
        /// Calculates the damage of a warrior.
        /// Warrior damage scales off their total STR stat.
        /// </summary>
        /// <returns>Double. Contains the damage based on total stat</returns>
        override public double CalculateDamage()
        {
            return 1.0 + (TotalPrimary.Strength / 100.0);
        }

        /// <summary>
        /// Creates a new warrior with the base stat attributes of any newly created level 1 warrior.
        /// 5 STR, 2 DEX, 1 INT, 10 VIT
        /// </summary>
        /// <param name="name">Name of the warrior</param>
        public Warrior(string name) : base(name)
        {
            this.BasePrimary = new PrimaryAttribute(5,2,1,10);
        }

    }
}
