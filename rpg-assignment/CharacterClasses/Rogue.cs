﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_assignment
{
    public class Rogue : Character
    {
        /// <summary>
        /// Returns the class-name of the character
        /// </summary>
        public override string ClassName => "Rogue";

        /// <summary>
        /// Defines the attributes earned on level-up of a rogue.
        /// 1 STR, 4 DEX, 1INT, 3 VIT
        /// </summary>
        public override PrimaryAttribute GrowthRate => new(1, 4, 1, 3);

        /// <summary>
        /// Defines the allowed weapon types of a rogue
        /// Daggers and Swords
        /// </summary>
        public override List<WeaponType> AllowedWeaponTypes
        {
            get
            {
                return new List<WeaponType> { WeaponType.DAGGER, WeaponType.SWORD };
            }
        }

        /// <summary>
        /// Defines the allowed armor types for a rogue
        /// Leather and Mail
        /// </summary>
        public override List<ArmorType> AllowedArmorTypes
        {
            get
            {
                return new List<ArmorType> { ArmorType.LEATHER, ArmorType.MAIL };
            }
        }

        /// <summary>
        /// Calculates the damage of a rogue
        /// Rogue damage scales off their total DEX stat
        /// </summary>
        /// <returns>Double. Contains the damage based on total stat</returns>
        public override double CalculateDamage()
        {
            return 1.0 + (TotalPrimary.Dexterity / 100.0);
        }


        /// <summary>
        /// Creates a new rogue with the base stat attributes of any newly created level 1 rogue
        /// 2 STR, 6 DEX, 1 INT, 8 VIT
        /// </summary>
        /// <param name="name">Name of the rogue character</param>
        public Rogue(string name) : base(name)
        {
            this.BasePrimary = new PrimaryAttribute(2, 6, 1, 8);
        }
    }
}
