﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_assignment
{
    public class Mage : Character
    {
        /// <summary>
        /// Returns the class-name of the character
        /// </summary>
        public override string ClassName => "Mage";

        /// <summary>
        /// Defines the attributes earned on level-up of a mage.
        /// 1 STR, 1 DEX, 5 INT, 3 VIT
        /// </summary>
        public override PrimaryAttribute GrowthRate => new(1, 1, 5, 3);

        /// <summary>
        /// Defines the allowed weapons of a mage.
        /// Staffs and Wands
        /// </summary>
        public override List<WeaponType> AllowedWeaponTypes
        {
            get
            {
                return new List<WeaponType> { WeaponType.STAFF, WeaponType.WAND };
            }
        }

        /// <summary>
        /// Defines the allowed armor types for a mage
        /// Cloth only
        /// </summary>
        public override List<ArmorType> AllowedArmorTypes
        {
            get
            {
                return new List<ArmorType> { ArmorType.CLOTH };
            }
        }

        /// <summary>
        /// Calculates damage of a mage.
        /// Mage damage scales off their total INT stat
        /// </summary>
        /// <returns>Double. Contains the damage based on total stat</returns>
        public override double CalculateDamage()
        {
            return 1.0 + (TotalPrimary.Intelligence / 100.0);
        }

        /// <summary>
        /// Creates a new mage with the base stat attributes of any newly created level 1 Mage.
        /// 1 STR, 1 DEX, 8 INT, 5 VIT
        /// </summary>
        /// <param name="name">Name of the mage character</param>
        public Mage(string name) : base(name)
        {
            this.BasePrimary = new PrimaryAttribute(1, 1, 8, 5);
        }
    }
}
