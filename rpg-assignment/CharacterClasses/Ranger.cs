﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_assignment
{
    public class Ranger : Character
    {

        /// <summary>
        /// Returns the class-name of the character
        /// </summary>
        public override string ClassName => "Ranger";

        /// <summary>
        /// Defines the attributes earned on level-up of a ranger.
        /// 1 STR, 5 DEX, 1 INT, 2 VIT
        /// </summary>
        public override PrimaryAttribute GrowthRate => new(1, 5, 1, 2);

        /// <summary>
        /// Defines the allowed weapons of a ranger.
        /// Bows only
        /// </summary>
        public override List<WeaponType> AllowedWeaponTypes
        {
            get
            {
                return new List<WeaponType> { WeaponType.BOW };
            }
        }

        /// <summary>
        /// Defines the allowed armor types for a ranger
        /// Leather and Mail
        /// </summary>
        public override List<ArmorType> AllowedArmorTypes
        {
            get
            {
                return new List<ArmorType> { ArmorType.LEATHER, ArmorType.MAIL };
            }
        }

        /// <summary>
        /// Calculates the damage of a ranger
        /// Ranger damage scales off their total DEX stat
        /// </summary>
        /// <returns>Double. Contains the damage based on total stat</returns>
        public override double CalculateDamage()
        {
            return 1.0 + (TotalPrimary.Dexterity / 100.0);
        }

        /// <summary>
        /// Creates a new ranger with the base stat attributes of any newly created leve 1 Ranger.
        /// 1 STR, 7 DEX, 1 INT, 8 VIT
        /// </summary>
        /// <param name="name">Name of the ranger</param>
        public Ranger(string name) : base(name)
        {
            this.BasePrimary = new PrimaryAttribute(1, 7, 1, 8);
        }
    }
}
