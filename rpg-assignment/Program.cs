﻿using System;

namespace rpg_assignment
{
    class Program
    {
        // Note that anything in this file is not intended to be part of my assignment delivery. Just something that is available to run for fun.

        static void Main(string[] args)
        {
            PrintIntroduction();

            Character hero;
            string selectedClass = RequestClass();
            string name = RequestName();
            switch (selectedClass)
            {
                case "warrior":
                    hero = new Warrior(name);
                    break;
                case "rogue":
                    hero = new Rogue(name);
                    break;
                case "ranger":
                    hero = new Ranger(name);
                    break;
                case "mage":
                    hero = new Mage(name);
                    break;
                default:
                    hero = new Rogue("Somehow you got here without making a proper hero, so now this is your name and you're a rogue now. Deal with it.");
                    break;
            }

            PrintHeroIntro(hero);

            StartAdventure(hero);

            Console.ForegroundColor = ConsoleColor.Green;
            System.Threading.Thread.Sleep(300);
            Console.WriteLine("Thank you for playing! Your character has been deleted from the universe never to be seen again!");
            System.Threading.Thread.Sleep(1000);
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("======== GAME END ==========");
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// Prints out the introduction sequence at the start of the game
        /// </summary>
        static void PrintIntroduction()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine("!!=== Welcome to ExtremelyBareBonesRPG! ===!!");
            Console.ForegroundColor = ConsoleColor.Gray;
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine("In this RPG, you make a character, equip weapons and armor, and swing at a test-dummy to see your DPS. That's about it.");
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine("We give you the option to go adventuring and grind to level up, but to save you time, it all happens instantly. Convenient right?");
            System.Threading.Thread.Sleep(1000);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Which class do you want to play? (Warrior / Rogue / Ranger / Mage)");
            Console.ForegroundColor = ConsoleColor.Magenta;
        }

        /// <summary>
        /// Asks the user about what class they want to play
        /// </summary>
        /// <returns>String representing class in lowercase</returns>
        static string RequestClass()
        {
            string response = Console.ReadLine().ToLower();
            if(response == "warrior" || response == "rogue" || response == "ranger" || response == "mage")
            {
                return response;
            } else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                System.Threading.Thread.Sleep(300);
                Console.WriteLine("That's not one of the available classes, please write one of the following: 'Warrior', 'Rogue', 'Ranger','Mage'");
                Console.ForegroundColor = ConsoleColor.Magenta;
                return RequestClass();
            }
        }

        /// <summary>
        /// Asks the user about what their hero's name will be
        /// </summary>
        /// <returns>String representing name of hero</returns>
        static string RequestName()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            System.Threading.Thread.Sleep(300);
            Console.WriteLine("Next, what is your hero's name going to be? This really doesn't matter in the end so write whatever!");
            Console.ForegroundColor = ConsoleColor.Magenta;
            return Console.ReadLine();
        }

        /// <summary>
        /// Prints out the stats of the hero with a welcome message
        /// </summary>
        /// <param name="hero">Hero's stats to be printed</param>
        static void PrintHeroIntro(Character hero)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            System.Threading.Thread.Sleep(300);
            Console.WriteLine($"== Welcome hero {hero.Name}! ==");
            System.Threading.Thread.Sleep(1000);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(hero.ToString());
        }

        /// <summary>
        /// Begins the "Game loop" and handles decisions 
        /// </summary>
        /// <param name="hero">Player hero</param>
        static void StartAdventure(Character hero)
        {
            bool gameOn = true;
            while (gameOn)
            {
                System.Threading.Thread.Sleep(1000);
                switch (RequestAction())
                {
                    case 1: // Show character card
                        PrintHeroStats(hero);
                        break;
                    case 2: // Attack target dummy
                        PrintHeroDamage(hero);
                        break;
                    case 3: // Go grinding for levels
                        GrindHero(hero);
                        break;
                    case 4: // Go hunting for items
                        HuntForItem(hero);
                        break;
                    case 0: // Quit game
                        gameOn = false;
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Invalid option selected: Please select one of the numbers provided");
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                }
            }
        }

        /// <summary>
        /// Asks for player to select an action in the main loop
        /// </summary>
        /// <returns>an integer representing the chosen option</returns>
        static int RequestAction()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("== ACTION SELECTION: What would you like to do? ==");
            Console.WriteLine("Select action by writing one of the numbers:");
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("" +
                "- 1: Display character stats\n" +
                "- 2: Attack a target-dummy to test your powers\n" +
                "- 3: Go grinding for levels\n" +
                "- 4: Go hunting for items and equipment\n" +
                "---\n" +
                "- 0: Quit the game");
            Console.ForegroundColor = ConsoleColor.Magenta;
            try
            {
                return int.Parse(Console.ReadLine());
            } catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                System.Threading.Thread.Sleep(200);
                Console.WriteLine("Not a valid input for action selection. Please try again..");
                return RequestAction();
            }
        }

        /// <summary>
        /// Prints out the hero's stats
        /// </summary>
        /// <param name="hero">Hero to print out</param>
        static void PrintHeroStats(Character hero)
        {
            System.Threading.Thread.Sleep(400);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(hero);
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// Prints out an "attack sequence
        /// </summary>
        /// <param name="hero">Attacking hero</param>
        static void PrintHeroDamage(Character hero)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            System.Threading.Thread.Sleep(200);
            Console.WriteLine($"{hero.Name} attacks a target dummy in whatever way a {hero.ClassName} attacks!!");
            Console.ForegroundColor = ConsoleColor.Green;
            double damageDealt = hero.Attack();
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine($"{hero.Name} dealt {damageDealt} damage to the target dummy!");
            System.Threading.Thread.Sleep(1000);
            Console.ForegroundColor = ConsoleColor.Red;
            if (damageDealt < 5)
            {
                Console.WriteLine("The target dummy appears completely unharmed.");
            } 
            else if (damageDealt >= 5 && damageDealt < 20)
            {
                Console.WriteLine("The fabric of the dummy is slightly torn.");
            }
            else if (damageDealt >= 20 && damageDealt < 40)
            {
                Console.WriteLine("The dummy is knocked over!");
            }
            else if (damageDealt >= 40 && damageDealt < 60)
            {
                Console.WriteLine("The dummy is broken apart!");
            }
            else if (damageDealt == 69)
            {
                Console.WriteLine("The dummy suddenly starts speaking and says... 'nice..' Then its destroyed.");
            }
            else if (damageDealt >= 60 && damageDealt < 100)
            {
                Console.WriteLine("The target dummy is shattered and it's pieces sent flying.");
            }
            else if (damageDealt == 420)
            {
                Console.WriteLine("The target dummy is pretty blazed. Then it explodes.");
            }
            else if (damageDealt >= 100)
            {
                Console.WriteLine("Target dummy just straight up explodes");
            }
            else
            {
                Console.WriteLine("Something happened to the dummy, but I don't know what.");
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// Levels up a hero
        /// </summary>
        /// <param name="hero">Hero to level up</param>
        static void GrindHero (Character hero)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            System.Threading.Thread.Sleep(200);
            Console.WriteLine($"{hero.Name} heads out of town and coldheartedly kills everything in sight..");
            hero.LevelUp();
            Console.ForegroundColor = ConsoleColor.Green;
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine($"{hero.Name} has leveled up!!");
            Console.ForegroundColor = ConsoleColor.Yellow;
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine($"{hero.Name} is now level {hero.Level}");
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// Lets the hero equip a random item or not
        /// </summary>
        /// <param name="hero">Hero to search for item</param>
        static void HuntForItem (Character hero)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            System.Threading.Thread.Sleep(200);
            Console.WriteLine($"{hero.Name} goes hunting for new equipment...");
            Console.ForegroundColor = ConsoleColor.Green;
            System.Threading.Thread.Sleep(1000);
            Item item = GetRandomItem();
            Console.WriteLine($"{hero.Name} discovered a new item!!");
            Console.ForegroundColor = ConsoleColor.Yellow;
            System.Threading.Thread.Sleep(100);
            Console.WriteLine(item);
            System.Threading.Thread.Sleep(600);
            if(ConfirmEquip())
            {
                try
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    System.Threading.Thread.Sleep(600);
                    if (item.GetType() == typeof(Armor)) // Spaghetti Check :)
                    {
                        Console.WriteLine(hero.Equip((Armor)item));
                    } else
                    {
                        Console.WriteLine(hero.Equip((Weapon)item));
                    }
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    System.Threading.Thread.Sleep(600);
                    Console.WriteLine(ex.Message);
                    System.Threading.Thread.Sleep(600);
                    Console.WriteLine("The item was tossed away instead..");
                }
            } else
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                System.Threading.Thread.Sleep(600);
                Console.WriteLine("The item was left behind.");
            }

            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// Asks for confirmation on item equip
        /// </summary>
        /// <returns>Boolean, yes or no</returns>
        static bool ConfirmEquip()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Would you like to equip the item? ['y' / 'n']");
            Console.ForegroundColor = ConsoleColor.Magenta;
            string response = Console.ReadLine().ToLower();
            if(response == "y")
            {
                return true;
            } else if (response == "n")
            {
                return false;
            } else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                System.Threading.Thread.Sleep(300);
                Console.WriteLine("Invalid response..");
                System.Threading.Thread.Sleep(300);
                return ConfirmEquip();
            }
        }

        /// <summary>
        /// Defines the items available in the "game"
        /// </summary>
        /// <returns>a random weapon or armor piece</returns>
        static Item GetRandomItem()
        {
            Item[] items =
            {
                new Weapon("Common Sword",      WeaponType.SWORD,   new(7, 1.2),    1),
                new Weapon("Common Axe",        WeaponType.AXE,     new(8, 1.1),    1),
                new Weapon("Common Hammer",     WeaponType.HAMMER,  new(9, 1.0),    1),
                new Weapon("Common Staff",      WeaponType.STAFF,   new(7, 1.1),    1),
                new Weapon("Common Wand",       WeaponType.WAND,    new(6, 1.2),    1),
                new Weapon("Common Bow",        WeaponType.BOW,     new(7, 1.1),    1),
                new Weapon("Common Dagger",     WeaponType.DAGGER,  new(5, 1.3),    1),

                new Weapon("Steel Sword",       WeaponType.SWORD,   new(10, 1.2),   2),
                new Weapon("Steel Axe",         WeaponType.AXE,     new(12, 1.1),   2),
                new Weapon("Steel Hammer",      WeaponType.HAMMER,  new(13, 1.0),   2),
                new Weapon("Amethyst Staff",    WeaponType.STAFF,   new(10, 1.1),   2),
                new Weapon("Amethyst Wand",     WeaponType.WAND,    new(9, 1.2),    2),
                new Weapon("Hardwood Bow",      WeaponType.BOW,     new(11, 1.1),   2),
                new Weapon("Steel Dagger",      WeaponType.DAGGER,  new(8, 1.3),    2),
                
                new Weapon("Mythril Sword",     WeaponType.SWORD,   new(21, 1.3),   5),
                new Weapon("Mythril Axe",       WeaponType.AXE,     new(23, 1.2),   5),
                new Weapon("Mythril Hammer",    WeaponType.HAMMER,  new(25, 1.1),   5),
                new Weapon("Sapphire Staff",    WeaponType.STAFF,   new(23, 1.2),   5),
                new Weapon("Sapphire Wand",     WeaponType.WAND,    new(20, 1.3),   5),
                new Weapon("Recurve Bow",       WeaponType.BOW,     new(24, 1.2),   5),
                new Weapon("Mythril Dagger",    WeaponType.DAGGER,  new(19, 1.4),   5),
                                
                new Weapon("Demonbane Sword",     WeaponType.SWORD,   new(42, 1.4),   10),
                new Weapon("Demonbane Axe",       WeaponType.AXE,     new(45, 1.3),   10),
                new Weapon("Demonbane Hammer",    WeaponType.HAMMER,  new(47, 1.2),   10),
                new Weapon("Unholy Staff",        WeaponType.STAFF,   new(46, 1.3),   10),
                new Weapon("Unholy Wand",         WeaponType.WAND,    new(42, 1.4),   10),
                new Weapon("Lightsplitter Bow",   WeaponType.BOW,     new(44, 1.3),   10),
                new Weapon("Crimson Edge Dagger", WeaponType.DAGGER,  new(40, 1.6),   10),
                                                
                new Weapon("Arcane Terra-blade",        WeaponType.SWORD,   new(82, 1.5),   30),
                new Weapon("Arcane Mercurial Axe",      WeaponType.AXE,     new(85, 1.4),   30),
                new Weapon("Arcane Sunsteel Crusher",   WeaponType.HAMMER,  new(87, 1.3),   30),
                new Weapon("Staff of Hope",             WeaponType.STAFF,   new(86, 1.4),   30),
                new Weapon("Wand of Dreams",            WeaponType.WAND,    new(82, 1.5),   30),
                new Weapon("Bow of Odysseus",           WeaponType.BOW,     new(84, 1.4),   30),
                new Weapon("Arcane Umbra Dagger",       WeaponType.DAGGER,  new(80, 1.8),   30),
                                                                
                // Ultimate weapons (Level 50 requirement)
                new Weapon("Ultima",            WeaponType.SWORD,   new(170, 2.0),   50),
                new Weapon("Elemental Staff",   WeaponType.STAFF,   new(200, 1.5),   50),
                new Weapon("Gun",               WeaponType.BOW,     new(130, 2.5),   50),
                new Weapon("1000 Degree Knife", WeaponType.DAGGER,  new(100, 5.0),   50),


                // Armor is boring so I didn't make more than two tiers :) Also I cant be asked to try and balance. Tier 1 is basic, tier 2 is just 10* the stats.
                new Armor("Common Cloth Hat",               ArmorType.CLOTH,   ItemSlot.HEAD,      new(0,0,5,2), 1),
                new Armor("Common Cloth Top",               ArmorType.CLOTH,   ItemSlot.BODY,      new(0,0,4,3), 1),
                new Armor("Common Cloth Leggings",          ArmorType.CLOTH,   ItemSlot.LEGS,      new(0,0,3,2), 1),
                new Armor("Common Leather Cap",             ArmorType.LEATHER,   ItemSlot.HEAD,    new(0,3,0,2), 1),
                new Armor("Common Leather Jacket",          ArmorType.LEATHER,   ItemSlot.BODY,    new(0,2,0,4), 1),
                new Armor("Common Leather Pants",           ArmorType.LEATHER,   ItemSlot.LEGS,    new(0,1,0,2), 1),
                new Armor("Common Chainmail Headgear",      ArmorType.MAIL,   ItemSlot.HEAD,       new(2,2,0,3), 1),
                new Armor("Common Chainmail Top",           ArmorType.MAIL,   ItemSlot.BODY,       new(3,3,0,5), 1),
                new Armor("Common Chainmail Leggings",      ArmorType.MAIL,   ItemSlot.LEGS,       new(2,2,0,4), 1),
                new Armor("Common Plate Helmet",            ArmorType.PLATE,   ItemSlot.HEAD,      new(2,0,0,5), 1),
                new Armor("Common Plate Armor",             ArmorType.PLATE,   ItemSlot.BODY,      new(3,0,0,5), 1),
                new Armor("Common Plate Leggings",          ArmorType.PLATE,   ItemSlot.LEGS,      new(1,0,0,5), 1),                
                
                new Armor("Tattered Wizard Hat",            ArmorType.CLOTH,   ItemSlot.HEAD,      new(0,0,50,20), 10),
                new Armor("Magician's Robe",                ArmorType.CLOTH,   ItemSlot.BODY,      new(0,0,40,30), 10),
                new Armor("Pants of pure magic",            ArmorType.CLOTH,   ItemSlot.LEGS,      new(0,0,30,20), 10),
                new Armor("Stealth Goggles",                ArmorType.LEATHER,   ItemSlot.HEAD,    new(0,30,0,20), 10),
                new Armor("Hunter's Outfit",                ArmorType.LEATHER,   ItemSlot.BODY,    new(0,20,0,40), 10),
                new Armor("Hunter's Shoes",                 ArmorType.LEATHER,   ItemSlot.LEGS,    new(0,10,0,20), 10),
                new Armor("Steel Chainmail Headgear",       ArmorType.MAIL,   ItemSlot.HEAD,       new(20,20,0,30), 10),
                new Armor("Steel Chainmail Top",            ArmorType.MAIL,   ItemSlot.BODY,       new(30,30,0,50), 10),
                new Armor("Steel Chainmail Leggings",       ArmorType.MAIL,   ItemSlot.LEGS,       new(20,20,0,40), 10),
                new Armor("Mythril Plate Helmet",           ArmorType.PLATE,   ItemSlot.HEAD,      new(20,0,0,50), 10),
                new Armor("Mythril Plate Armor",            ArmorType.PLATE,   ItemSlot.BODY,      new(30,0,0,50), 10),
                new Armor("Mythril Plate Leggings",         ArmorType.PLATE,   ItemSlot.LEGS,      new(10,0,0,50), 10)
            };

            Random rand = new();
            return items[rand.Next(0, items.Length)];
        }
    }
}
